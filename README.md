# Fuzzmoji

Quickly find Emoji with Fuzzel! 😎, This is a shameless fork of [rofmoji](https://codeberg.org/arran/rofmoji) by [arran](https://codeberg.org/arran)

![Fuzzmoji](./media/fuzzmoji.gif)

## Installation

If you are using gentoo, you can find this package in my [cowaybuilds](https://codeberg.org/codingotaku/cowaybuilds) ebuild repository. For generic linux installation, do the following.

### 0. Clone this repository

```
git clone https://codeberg.org/codingotaku/fuzzmoji.git
```

### 1. Install the dependencies
Go through the [dependencies](#dependencies) section, install the packages you need.

### 2. Run the following

```
cd fuzzmoji

# Copy emoji-list to a shared folder
sudo cp emoji-list /usr/share/fuzzmoji/emoji-list

# Copy fuzzmoji script to bin folder
sudo cp fuzzmoji /usr/bin/fuzzmoji
```

### 3. Bind `fuzzmoji` to a key! 🥳

```
# works on sway or i3wm, read the documentation for other Window manager or Desktop environment
bindsym $mod+i exec fuzzmoji
```

## Usage

Just type keywords to search for an Emoji
Emoji Modifiers are also available, Include one or more of the following in your query:

#### Skin tone
`light, medium-light, medium, medium-dark, dark`

#### Gender
`man, woman, gender-neutral`

#### Hair
`red, curly, blonde, white, bald`

### Examples

- 👱🏾‍♂ - `medium-dark bond man`
- 👩🏾‍⚕ - `dark woman doctor`
- 👨🏾‍🚒 - `dark firefighter man`
- 👩🏻‍🦰 - `light red woman`
- 🧑🏾 - `gender-neutral dark adult`
- 🧙🏼 - `witch medium`
- 👮‍♀ - `police woman`
- 👨‍💻 - `coder man`
- 👌 - `ok hand`
- 🐸 - `frog`
- 🤯 - `exploding head`

## Dependencies

- [fuzzel](https://codeberg.org/dnkl/fuzzel)
- [libnotify](https://gitlab.gnome.org/GNOME/libnotify)
- [wl-clipboard](https://github.com/bugaevc/wl-clipboard) (optional, prioritized)`

If `wl-clipboard` is not present, the emoji is output to the terminal, you should be able to pipe it to the clipboard that you are using.

## Contributing

If you have a suggestion to improve `fuzzmoji`, such as Emoji keywords, please make a Pull Request or send the patch to me via an email.
